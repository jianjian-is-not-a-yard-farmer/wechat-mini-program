let app = getApp();
Page({
  data: {
    
    submitted:false
  },
  formSubmit: function (e) {
    console.log('form发生了submit事件，携带数据为：', e.detail.value.song, e.detail.value.singer);
    this.setData({
      submitted:true
    })
  },
  formReset: function () {
    console.log('form发生了reset事件')
    this.setData({
      submitted:false
    })
  }
})