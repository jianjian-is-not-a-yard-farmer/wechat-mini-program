import {Controller, Get, Post, Body, HttpCode, HttpStatus} from '@nestjs/common';
import {ApiOperation, ApiTags} from "@nestjs/swagger";

@Controller('methods')
export class MethodsController {
  @Get('testGet')
  getExample(): string {
    return '简单测试http申请协议get';
  }

  @ApiOperation({ summary: '测试post', description: '测试Post' })
  @Post('testPost')
  @HttpCode(HttpStatus.CREATED)
  postExample(@Body() body: any): string {
    console.log('Received body:', body);
    return '收到并处理body.'
  }
}
