import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {RouterModule} from '@nestjs/core';
import {MethodsController} from "@src/modules/Methods/controllers/methods.controller";

@Module({
  imports: [
    RouterModule.register([
      {
        path: 'req',
        module: MethodsModule,
      },
    ]),
    TypeOrmModule.forFeature([]),
  ],
  controllers: [MethodsController],
  providers: [],
})
export class MethodsModule {
}
