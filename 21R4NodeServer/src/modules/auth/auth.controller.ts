import { Body, Controller,Post, UseGuards } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { ApiPropertyOptional, ApiTags } from "@nestjs/swagger";
import { WxService } from "../wx/wx.service";

export class WxLoginInfo {
    @ApiPropertyOptional({ required: true, description: '用户名' })
    readonly userName: string;
  
    @ApiPropertyOptional({ required: false, description: '密码' })
    readonly userPassword: string;
  
    @ApiPropertyOptional({ required: false, description: '微信登录code' })
    readonly loginCode: string;
  }
  

@ApiTags('权限')
@Controller('auth')
export default class AuthController {
    constructor(
        private readonly authService: AuthService,
        private readonly wxService: WxService
    ) { }

    @Post('login_by_Name')
    async loginByName(@Body() payload: WxLoginInfo): Promise<any> {
        console.log("######loginByName:",payload )
        const user = await this.authService.validateUserByName(payload);
        console.log("######user:",user )
        const accessToken =  await this.authService.createToken(user);
        const openId = await this.wxService.code2Session(payload.loginCode)
        return {
            accessToken,
            openId
        }
    }

}

