import { Injectable } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { UserEntity } from '../User/User.entity';



@Injectable()
export class JwtService {
    async sign(payload: UserEntity): Promise<string> {
        const user = JSON.stringify(payload);
        return jwt.sign({ user }, process.env.JWT_SECRET_KEY || '', { expiresIn: process.env.JWT_EXPIRATION_TIME });
    }

    async verify(token: string): Promise<any> {
        return jwt.verify(token, process.env.JWT_SECRET_KEY || '');
    }
}

