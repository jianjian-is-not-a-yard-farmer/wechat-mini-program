import { Module } from '@nestjs/common';
import AuthController from './auth.controller';
import { JwtService } from './jwt.service';
import { AuthService } from './auth.service';
import { UserModule } from '../User/User.module';
import { WxModule } from '../wx/wx.module';


@Module({
    imports: [UserModule, WxModule],
    controllers: [
        AuthController,
    ],
    providers: [
        AuthService,
        JwtService,
    ],
    exports: [
        JwtService,
        AuthService
    ],
})
export class AuthModule { }