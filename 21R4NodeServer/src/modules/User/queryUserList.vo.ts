import { ApiProperty } from '@nestjs/swagger';
import { UserEntity } from './User.entity';



export class QueryUserListVo  {
  @ApiProperty({ description: '数据', type: UserEntity, isArray: true })
  data: UserEntity[];
}
