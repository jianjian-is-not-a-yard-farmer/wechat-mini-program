import { ApiPropertyOptional } from "@nestjs/swagger"
import { Entity, PrimaryGeneratedColumn, Column } from "typeorm"

@Entity("user")
export class UserEntity {

    @PrimaryGeneratedColumn()
    id: string

    @ApiPropertyOptional({ description: '微信ID' })
    @Column({
        type: 'varchar',
        nullable: false,
        length: 128,
        name: 'wxID',
        comment: '微信ID',
    })
    wxID: string

    @ApiPropertyOptional({ description: '用户名称' })
    @Column({
        type: 'varchar',
        nullable: false,
        length: 128,
        name: 'userName',
        comment: '用户名称',
    })
    userName: string


    @ApiPropertyOptional({ description: '登录密码' })
    @Column({
        type: 'varchar',
        nullable: true,
        length: 128,
        name: 'userPassword',
        comment: '登录密码',
    })
    userPassword?: string


    @Column()
    isActive: boolean
    
}