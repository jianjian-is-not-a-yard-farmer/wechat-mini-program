import { forwardRef, Global, Module } from '@nestjs/common';
import { RouterModule } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './User.entity';
import { UserController } from './User.controller';
import { UserService } from './User.service';
import { WxModule } from '../wx/wx.module';


@Module({
  imports: [
    RouterModule.register([
      {
        path: 'user',
        module: this,
      }
    ]),
    TypeOrmModule.forFeature([UserEntity]),
    WxModule
  ],
  controllers: [UserController],
  providers: [UserService],
  exports: [UserService],
})
export class UserModule { }
