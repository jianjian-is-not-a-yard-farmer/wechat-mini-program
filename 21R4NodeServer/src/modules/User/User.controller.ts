import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UserService } from './User.service';
import { QueryUserListVo } from './queryUserList.vo';
import { JwtAuthGuard } from '@src/guard/auth.guard';
import { WxService } from '../wx/wx.service';
import { CurrentUser } from '@src/decorators/current.user';
import { UserDto } from './interface';

@ApiTags('用户管理')
@Controller('user')
export class UserController {

  constructor(
    private readonly userService: UserService,
    private readonly wxService: WxService
  ) {
  }


  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: '查询单个用户', description: '查询单个用户' })
  @ApiOkResponse({
    type: QueryUserListVo,
    description: '查询单个用户',
  })
  @Get('query/:userID')
  @HttpCode(HttpStatus.CREATED)
  async queryUser(@Param('userID') userID: string) {
    return this.userService.findOne(userID).then((data) => ({ data }));
  }

  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: '查询所有用户', description: '查询所有用户' })
  @ApiOkResponse({
    type: QueryUserListVo,
    description: '查询所有用户',
  })
  @Get('queryAll')
  @HttpCode(HttpStatus.CREATED)
  async queryAllUser() {
    return this.userService.findAll().then((data) => ({ data }));
  }

  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: '查询用户电话号码', description: '查询用户电话号码' })
  @ApiOkResponse({
    type: "",
    description: '查询用户电话号码',
  })
  @Get('query_user_phone_number')
  @HttpCode(HttpStatus.CREATED)
  async queryUserPhoneNumber(@Query('tmpCode') tmpCode: string) {
    return this.wxService.getUserPhoneNumber(tmpCode);
  }


  /**
   * 添加用户
   */
  // @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: '添加用户', description: '添加用户' })
  @Post('addAUser')
  @HttpCode(HttpStatus.CREATED)
  async addAUser(@Body() aUser: UserDto): Promise<any> {
    console.log('aUser controller:', aUser);
    return this.userService.addAUser(aUser);
  }


  
  /**
   * 根据id删除用户
   */
  // @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: '根据用户ID删除用户', description: '根据用户ID删除用户' })
  @Get('delete/:userID')
  @HttpCode(HttpStatus.OK)
  async deleteCommentsByCommentID(@Param('userID') userID: string): Promise<any> {
    return this.userService.deleteUserByID(userID);
  }


  
}
