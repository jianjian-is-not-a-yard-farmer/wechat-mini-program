import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {RouterModule} from '@nestjs/core';
import {LoadController} from "@src/modules/Load/controllers/load.controller";

@Module({
  imports: [
    RouterModule.register([
      {
        path: 'load',
        module: LoadModule,
      },
    ]),
    TypeOrmModule.forFeature([]),
  ],
  controllers: [LoadController],
  providers: [],
})

export class LoadModule {
}
