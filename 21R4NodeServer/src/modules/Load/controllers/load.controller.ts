import {Controller, Get, Res} from '@nestjs/common';
import {ApiOperation} from "@nestjs/swagger";
import type {Response} from 'express'


@Controller('load')
export class LoadController {
  @ApiOperation({summary: '测试下载图片', description: '下载public/image中的图片'})
  @Get('download')
  download(@Res() res: Response) {
    const url = 'public/images/test.png'
    res.download(url)
  }
}
