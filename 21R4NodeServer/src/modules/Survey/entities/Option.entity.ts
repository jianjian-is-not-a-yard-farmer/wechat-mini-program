import { ApiPropertyOptional } from '@nestjs/swagger';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { QuestionEntity } from './Question.entity';

@Entity('option')
export class OptionEntity {

  @PrimaryGeneratedColumn()
  id: string;

  @ApiPropertyOptional({ description: '指向问题表，一对多' })
  @ManyToOne(() => QuestionEntity, question => question.id)
  @Column({
    type: 'varchar',
    nullable: false,
    length: 255,
    comment: '指向问题表，一对多',
  })
  questionID: string;

  @ApiPropertyOptional({ description: '选项' })
  @Column({
    type: 'varchar',
    nullable: false,
    length: 255,
    name: 'optionText',
    comment: '选项',
  })
  optionText: string;

}
