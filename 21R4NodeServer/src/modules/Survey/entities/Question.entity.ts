import { ApiPropertyOptional } from '@nestjs/swagger';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { OptionEntity } from './Option.entity';

@Entity('question')
export class QuestionEntity {

  @PrimaryGeneratedColumn()
  @OneToMany(() => OptionEntity, option => option.questionID)
  id: string;

  @ApiPropertyOptional({ description: '问题' })
  @Column({
    type: 'varchar',
    nullable: false,
    length: 255,
    name: 'questionText',
    comment: '问题',
  })
  questionText: string;

  @ApiPropertyOptional({ description: '单选、多选、文本输入等' })
  @Column({
    type: 'int',
    nullable: false,
    name: 'QuestionType',
    comment: '单选、多选、文本输入等',
  })
  QuestionType: number;

}
