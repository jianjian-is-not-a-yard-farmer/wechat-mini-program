import {Controller, Get, Post, Body, HttpCode, HttpStatus} from '@nestjs/common';
import {ApiOperation, ApiTags} from "@nestjs/swagger";

@ApiTags('问卷调查')
@Controller('survey')
export class SurveyController {
  @ApiOperation({summary: '问卷调查', description: '查找产品价格，通过产品ID查找价格'})
  @Post('productId')
  @HttpCode(HttpStatus.CREATED)
  async queryProductInfo(@Body() body: any): Promise<{ id: string, productId: string, price: number }> {
    const {id, productId, com }  = body;
    console.log('Received body:', body, id, productId, com);
    return {
      id,
      productId,
      price:987.99
    }
  }
}
