import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RouterModule } from '@nestjs/core';
import { SurveyController } from './controllers/Survey.controller';
import { OptionEntity } from './entities/Option.entity';
import { QuestionEntity } from './entities/Question.entity';

@Module({
  imports: [
    RouterModule.register([
      {
        path: 'survey',
        module: SurveyModule,
      },
    ]),
    TypeOrmModule.forFeature([OptionEntity, QuestionEntity]),
  ],
  controllers: [SurveyController],
  providers: [],
})
export class SurveyModule {
}
