import { Controller, Get, Post, Body, HttpCode, HttpStatus, Param, Query } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { SurveyService } from '../services/Survey.service';
import { ResponseEntity } from '@src/modules/Survey/entities/Response.entity';

@ApiTags('调查问卷')
@Controller('survey')
export class SurveyController {

  constructor(private readonly surveyService: SurveyService) {
  }

  @ApiOperation({ summary: '获取问卷表下的问题和选项', description: '根据问卷id获取当前问卷表下的所有问题和选项' })
  @Post('findSurveyBySurveyId')
  @HttpCode(HttpStatus.CREATED)
  async findSurveyBySurveyId(@Body('surveyId') surveyId: string) {
    return await this.surveyService.findSurveyWithQuestionsAndOptions(surveyId);
  }

  @ApiOperation({ summary: '提交问卷数据', description: '提交问卷数据' })
  @Post('save/:SurveyId/:RespondentId')
  @HttpCode(HttpStatus.CREATED)
  async save(@Body("responses") responses: ResponseEntity[], @Param("SurveyId") SurveyId: string, @Param("RespondentId") RespondentId: string) {
    return await this.surveyService.saveSurvey(responses, SurveyId, RespondentId);
  }
}
