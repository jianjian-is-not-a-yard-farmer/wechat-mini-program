import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { SurveyEntity } from '../entities/Survey.entity';
import { QuestionEntity } from '../entities/Question.entity';
import { OptionEntity } from '../entities/Option.entity';


@Entity('response')
export class ResponseEntity {

  @PrimaryGeneratedColumn()
  id: string;

  @Column({
    type: 'varchar',
    nullable: false,
    comment: '指向问卷表的 ID',
  })
  SurveyId: string;


  @Column({
    type: 'varchar',
    nullable: false,
    comment: '指向问题表的 ID',
  })
  QuestionId: string;


  @Column({
    type: 'varchar',
    length: 255,
    nullable: true,
    comment: '指向选项表的 ID，如果问题是文本输入则为空',
  })
  OptionId: string;

  // @ManyToOne(() => SurveyEntity)
  // @JoinColumn({ name: 'SurveyId' })
  // survey: SurveyEntity;
  //
  // @ManyToOne(() => QuestionEntity)
  // @JoinColumn({ name: 'QuestionId' })
  // question: QuestionEntity;
  //
  // @ManyToOne(() => OptionEntity)
  // @JoinColumn({ name: 'OptionId' })
  // option: OptionEntity;

  @Column({
    type: 'varchar',
    nullable: false,
    comment: '回答者ID，如果需要追踪回答者的话',
  })
  RespondentId: string;

  @Column({
    type: 'text',
    nullable: true,
    comment: '文本回答，如果问题是文本输入则为空',
  })
  ResponseText: string;


  @Column({
    type: 'timestamp',
    nullable: true,
    comment: '回答时间',
  })
  ResponseDateTime: Date;
}
