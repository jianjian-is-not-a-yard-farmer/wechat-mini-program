import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { SurveyQuestionEntity } from '@src/modules/Survey/entities/SurveyQuestion.entity';

@Entity('survey')
export class SurveyEntity {

  @PrimaryGeneratedColumn()
  id: string;

  @Column({ length: 255, comment: '问卷标题' })
  surveyName: string;

  @Column({ length: 255, comment: '副标题' })
  surveySubtitle: string;

  @OneToMany(() => SurveyQuestionEntity, surveyQuestion => surveyQuestion.survey)
  questions: SurveyQuestionEntity[];
}
