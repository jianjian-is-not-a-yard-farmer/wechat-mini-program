import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {SurveyEntity} from './Survey.entity';
import {QuestionEntity} from './Question.entity';

@Entity('surveyquestion')
export class SurveyQuestionEntity {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({comment: '问卷头'})
  surveyId: string;

  @ManyToOne(() => SurveyEntity, survey => survey.questions)
  survey: SurveyEntity;

  @Column({comment: '问题项'})
  questionId: string;

  @ManyToOne(() => QuestionEntity, question => question.surveyQuestions)
  question: QuestionEntity;

  @Column({comment: '顺序'})
  surveyOrder: number;
}
