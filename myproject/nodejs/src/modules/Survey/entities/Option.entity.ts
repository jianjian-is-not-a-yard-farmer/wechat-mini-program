import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { QuestionEntity } from './Question.entity';

@Entity('option')
export class OptionEntity {

  @PrimaryGeneratedColumn()
  id: string;

  @Column({
    type: 'varchar',
    nullable: false,
    comment: '问题ID',
    name: 'questionID',
  })
  questionID: string;


  @Column({
    type: 'varchar',
    nullable: false,
    length: 255,
    comment: '选项',
    name: 'optionText',
  })
  optionText: string;

  @ManyToOne(() => QuestionEntity, question => question.options)
  @JoinColumn({ name: 'questionID' })
  question: QuestionEntity;

}
