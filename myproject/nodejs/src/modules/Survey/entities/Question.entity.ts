import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { OptionEntity } from './Option.entity';
import { SurveyQuestionEntity } from '../entities/SurveyQuestion.entity';

@Entity('question')
export class QuestionEntity {

  @PrimaryGeneratedColumn()
  id: string;

  @Column({
    type: 'varchar',
    nullable: false,
    length: 255,
    name: 'questionText',
    comment: '问题',
  })
  questionText: string;

  @Column({
    type: 'int',
    nullable: false,
    name: 'QuestionType',
    comment: '单选、多选、文本输入等',
  })
  questionType: number;

  @OneToMany(() => OptionEntity, option => option.question)
  options: OptionEntity[];

  @OneToMany(() => SurveyQuestionEntity, surveyQuestion => surveyQuestion.question)
  surveyQuestions: SurveyQuestionEntity[];

}
