import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RouterModule } from '@nestjs/core';
import { SurveyController } from './controllers/Survey.controller';
import { OptionEntity } from './entities/Option.entity';
import { QuestionEntity } from './entities/Question.entity';
import { SurveyEntity } from './entities/Survey.entity';
import { SurveyQuestionEntity } from './entities/SurveyQuestion.entity';
import { ResponseEntity } from './entities/Response.entity';
import { SurveyService } from './services/Survey.service';

@Module({
  imports: [
    RouterModule.register([
      {
        path: 'survey',
        module: this,
      },
    ]),
    TypeOrmModule.forFeature([OptionEntity, QuestionEntity, SurveyEntity, SurveyQuestionEntity, ResponseEntity]),
  ],
  controllers: [SurveyController],
  providers: [SurveyService],
  exports: [SurveyService],
})
export class SurveyModule {
}
