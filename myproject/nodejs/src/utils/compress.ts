import { inflate } from 'pako';

export function unzip(b64Data: Uint8Array) {
  if (b64Data) {
    const data = inflate(b64Data);
    let res = '';
    const chunk = 8 * 1024;
    if (data) {
      for (let i = 0; i < data.length / chunk; i++) {
        res += String.fromCharCode.apply(null, data.slice(i * chunk, (i + 1) * chunk));
      }
      return decodeURIComponent(res);
    }
  }
  return '';
}
