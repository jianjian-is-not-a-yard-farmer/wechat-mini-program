/// <reference path="./types/index.d.ts" />

// 定义安全距离类型
interface SafeDistance {
  top: number;
  bottom: number;
  left: number;
  right: number;
}

interface IAppOption {
  globalData: {
    safeDistance: SafeDistance;
  }
  userInfoReadyCallback?: WechatMiniprogram.GetUserInfoSuccessCallback,
}