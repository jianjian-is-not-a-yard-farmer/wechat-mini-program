// app.ts
App<IAppOption>({
  globalData: {
    safeDistance: {} as SafeDistance,
  },
  onLaunch() {
    // 调用系统信息API获取设备信息
    wx.getSystemInfo({
      success: (res) => {
        // 获取屏幕的宽度和高度
        const screenWidth = res.screenWidth;
        const screenHeight = res.screenHeight;

        // 获取安全区域的信息
        const safeArea = res.safeArea;

        // 计算安全距离
        const safeDistance = {
          top: safeArea.top,
          bottom: screenHeight - safeArea.bottom,
          left: safeArea.left,
          right: screenWidth - safeArea.right,
        };

        // 将安全距离存储在全局变量中
        this.globalData.safeDistance = safeDistance;
      },
      fail: (err) => {
        console.error("获取系统信息失败", err);
      },
    });
  },
});
