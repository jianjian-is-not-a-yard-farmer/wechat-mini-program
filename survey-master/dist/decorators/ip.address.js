"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IpAddress = void 0;
const common_1 = require("@nestjs/common");
exports.IpAddress = (0, common_1.createParamDecorator)((_data, ctx) => {
    const req = ctx.switchToHttp().getRequest();
    const rawIp = req.header('x-forwarded-for') || req.connection.remoteAddress || req.socket.remoteAddress;
    const ipAddress = rawIp ? rawIp.split(',')[0] : '';
    return ipAddress;
});
//# sourceMappingURL=ip.address.js.map