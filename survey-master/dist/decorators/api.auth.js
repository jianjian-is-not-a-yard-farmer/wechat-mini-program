"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiAuth = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("../config");
function ApiAuth() {
    return (0, common_1.applyDecorators)((0, common_1.SetMetadata)(config_1.API_AUTH_KEY, true));
}
exports.ApiAuth = ApiAuth;
//# sourceMappingURL=api.auth.js.map