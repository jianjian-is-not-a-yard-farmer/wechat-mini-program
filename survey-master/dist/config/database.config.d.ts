declare const _default: {
    type: string | undefined;
    host: string | undefined;
    port: number;
    database: string | undefined;
    username: string | undefined;
    password: string | undefined;
    logging: boolean;
};
export default _default;
