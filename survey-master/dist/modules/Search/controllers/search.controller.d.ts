export declare class SearchController {
    queryProductInfo(body: any): Promise<{
        id: string;
        productId: string;
        price: number;
    }>;
}
