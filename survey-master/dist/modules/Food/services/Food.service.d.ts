import { Repository } from 'typeorm';
import { BannerEntity } from '../entities/Banner.entity';
import { ConllectEntity } from '../entities/Conllect.entity';
import { DetailEntity } from '../entities/Detail.entity';
import { ListEntity } from '../entities/List.entity';
import { UserEntity } from '@src/modules/User/User.entity';
export declare class FoodService {
    private readonly listRepository;
    private readonly bannerRepository;
    private readonly conllectRepository;
    private readonly detailRepository;
    private readonly userRepository;
    constructor(listRepository: Repository<ListEntity>, bannerRepository: Repository<BannerEntity>, conllectRepository: Repository<ConllectEntity>, detailRepository: Repository<DetailEntity>, userRepository: Repository<UserEntity>);
    findAll(): Promise<BannerEntity[]>;
    findAllList(): Promise<ListEntity[]>;
    findAllConllect(userId: any): Promise<ConllectEntity[]>;
    findDetail(foodsid: any): Promise<DetailEntity[]>;
    searchFoods(query: any): Promise<ListEntity[]>;
    deleteCollect(userId: string, collectId: string): Promise<any>;
    addCollect(userId: string, collectData: Partial<ConllectEntity>): Promise<ConllectEntity>;
}
