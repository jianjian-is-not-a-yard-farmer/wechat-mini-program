"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FoodService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const Banner_entity_1 = require("../entities/Banner.entity");
const Conllect_entity_1 = require("../entities/Conllect.entity");
const Detail_entity_1 = require("../entities/Detail.entity");
const List_entity_1 = require("../entities/List.entity");
const User_entity_1 = require("../../User/User.entity");
let FoodService = class FoodService {
    constructor(listRepository, bannerRepository, conllectRepository, detailRepository, userRepository) {
        this.listRepository = listRepository;
        this.bannerRepository = bannerRepository;
        this.conllectRepository = conllectRepository;
        this.detailRepository = detailRepository;
        this.userRepository = userRepository;
    }
    async findAll() {
        const banner = await this.bannerRepository.find();
        return banner;
    }
    async findAllList() {
        return await this.listRepository.find();
    }
    async findAllConllect(userId) {
        return this.conllectRepository.find({
            where: { user: { id: userId } },
            relations: ["user"]
        });
    }
    async findDetail(foodsid) {
        return await this.detailRepository.find({
            where: { id: foodsid }
        });
    }
    async searchFoods(query) {
        console.log('Received query:', query);
        return await this.listRepository
            .createQueryBuilder('foods')
            .where('foods.foods_title LIKE :query', { query: `%${query}%` })
            .getMany();
    }
    async deleteCollect(userId, collectId) {
        const detalecollect = await this.conllectRepository.delete({ id: collectId, user: { id: userId } });
        return detalecollect;
    }
    async addCollect(userId, collectData) {
        const user = await this.userRepository.findOne({ where: { id: userId } });
        if (!user) {
            throw new Error('User not found');
        }
        const collect = this.conllectRepository.create(Object.assign(Object.assign({}, collectData), { user }));
        return this.conllectRepository.save(collect);
    }
};
exports.FoodService = FoodService;
exports.FoodService = FoodService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(List_entity_1.ListEntity)),
    __param(1, (0, typeorm_1.InjectRepository)(Banner_entity_1.BannerEntity)),
    __param(2, (0, typeorm_1.InjectRepository)(Conllect_entity_1.ConllectEntity)),
    __param(3, (0, typeorm_1.InjectRepository)(Detail_entity_1.DetailEntity)),
    __param(4, (0, typeorm_1.InjectRepository)(User_entity_1.UserEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], FoodService);
//# sourceMappingURL=Food.service.js.map