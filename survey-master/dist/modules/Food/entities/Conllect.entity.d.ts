import { UserEntity } from '@src/modules/User/User.entity';
export declare class ConllectEntity {
    id: String;
    foods_id: string;
    foods_img: string;
    foods_title: string;
    foods_desc: string;
    name: string;
    user: UserEntity;
}
