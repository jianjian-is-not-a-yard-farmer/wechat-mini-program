"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DetailEntity = void 0;
const typeorm_1 = require("typeorm");
let DetailEntity = class DetailEntity {
};
exports.DetailEntity = DetailEntity;
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", String)
], DetailEntity.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'varchar',
        nullable: false,
        name: 'foods_id',
        comment: '美食id，美食详情页专属',
    }),
    __metadata("design:type", String)
], DetailEntity.prototype, "foods_id", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'varchar',
        nullable: false,
        name: 'foods_title',
        comment: '美食标题',
    }),
    __metadata("design:type", String)
], DetailEntity.prototype, "foods_title", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'varchar',
        nullable: false,
        name: 'foods_difficulty',
        comment: '烹饪难度',
    }),
    __metadata("design:type", String)
], DetailEntity.prototype, "foods_difficulty", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'varchar',
        nullable: false,
        name: 'foods_time',
        comment: '烹饪难度',
    }),
    __metadata("design:type", String)
], DetailEntity.prototype, "foods_time", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'varchar',
        nullable: false,
        name: 'foods_ingredient',
        comment: '食材',
    }),
    __metadata("design:type", String)
], DetailEntity.prototype, "foods_ingredient", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'varchar',
        nullable: false,
        name: 'foods_procedure',
        comment: '步骤',
    }),
    __metadata("design:type", String)
], DetailEntity.prototype, "foods_procedure", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'varchar',
        nullable: false,
        name: 'foods_img',
        comment: '图片',
    }),
    __metadata("design:type", String)
], DetailEntity.prototype, "foods_img", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'varchar',
        nullable: false,
        name: 'foods_htmlimg',
        comment: '图片分享',
    }),
    __metadata("design:type", String)
], DetailEntity.prototype, "foods_htmlimg", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'varchar',
        nullable: false,
        name: 'foods_knack',
        comment: '烹饪小窍门',
    }),
    __metadata("design:type", String)
], DetailEntity.prototype, "foods_knack", void 0);
exports.DetailEntity = DetailEntity = __decorate([
    (0, typeorm_1.Entity)('fooddetail')
], DetailEntity);
//# sourceMappingURL=Detail.entity.js.map