"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConllectEntity = void 0;
const typeorm_1 = require("typeorm");
const User_entity_1 = require("../../User/User.entity");
let ConllectEntity = class ConllectEntity {
};
exports.ConllectEntity = ConllectEntity;
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", String)
], ConllectEntity.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'varchar',
        nullable: false,
        name: 'foods_id',
        comment: '美食id，美食详情页专属',
    }),
    __metadata("design:type", String)
], ConllectEntity.prototype, "foods_id", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'varchar',
        nullable: false,
        name: 'foods_img',
        comment: '轮播图',
    }),
    __metadata("design:type", String)
], ConllectEntity.prototype, "foods_img", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'varchar',
        nullable: false,
        name: 'foods_title',
        comment: '美食标题',
    }),
    __metadata("design:type", String)
], ConllectEntity.prototype, "foods_title", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'varchar',
        nullable: false,
        name: 'foods_desc',
        comment: '简介',
    }),
    __metadata("design:type", String)
], ConllectEntity.prototype, "foods_desc", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'varchar',
        nullable: false,
        name: 'name',
        comment: '作者',
    }),
    __metadata("design:type", String)
], ConllectEntity.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => User_entity_1.UserEntity, user => user.collections),
    __metadata("design:type", User_entity_1.UserEntity)
], ConllectEntity.prototype, "user", void 0);
exports.ConllectEntity = ConllectEntity = __decorate([
    (0, typeorm_1.Entity)('conllect')
], ConllectEntity);
//# sourceMappingURL=Conllect.entity.js.map