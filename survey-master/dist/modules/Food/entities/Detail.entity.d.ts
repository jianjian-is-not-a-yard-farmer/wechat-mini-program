export declare class DetailEntity {
    id: String;
    foods_id: string;
    foods_title: string;
    foods_difficulty: string;
    foods_time: string;
    foods_ingredient: string;
    foods_procedure: string;
    foods_img: string;
    foods_htmlimg: string;
    foods_knack: string;
}
