"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FoodModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const core_1 = require("@nestjs/core");
const Banner_entity_1 = require("./entities/Banner.entity");
const Food_service_1 = require("../Food/services/Food.service");
const food_controller_1 = require("./controllers/food.controller");
const Conllect_entity_1 = require("./entities/Conllect.entity");
const User_entity_1 = require("../User/User.entity");
const Detail_entity_1 = require("./entities/Detail.entity");
const List_entity_1 = require("./entities/List.entity");
let FoodModule = class FoodModule {
};
exports.FoodModule = FoodModule;
exports.FoodModule = FoodModule = __decorate([
    (0, common_1.Module)({
        imports: [
            core_1.RouterModule.register([
                {
                    path: 'food',
                    module: this,
                },
            ]),
            typeorm_1.TypeOrmModule.forFeature([Banner_entity_1.BannerEntity, Conllect_entity_1.ConllectEntity, User_entity_1.UserEntity, Detail_entity_1.DetailEntity, List_entity_1.ListEntity]),
        ],
        controllers: [food_controller_1.FoodController],
        providers: [Food_service_1.FoodService],
        exports: [Food_service_1.FoodService],
    })
], FoodModule);
//# sourceMappingURL=food.module.js.map