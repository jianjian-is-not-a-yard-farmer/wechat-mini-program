"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FoodController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const Food_service_1 = require("../services/Food.service");
let FoodController = class FoodController {
    constructor(foodService) {
        this.foodService = foodService;
    }
    async bannerAll() {
        return this.foodService.findAll().then((data) => ({ data }));
    }
    async list() {
        return this.foodService.findAllList().then((data) => ({ data }));
    }
    async userconllect(userId) {
        const userIdNumber = parseInt(userId, 10);
        return this.foodService.findAllConllect(userIdNumber);
    }
    async detailfoods(foodsid) {
        return await this.foodService.findDetail(foodsid);
    }
    async searchFoods(query) {
        console.log('Received query:', query);
        return await this.foodService.searchFoods(query);
    }
    async deleteCollect(id, userId) {
        return await this.foodService.deleteCollect(userId, id);
    }
    async addCollect(userId, collectData) {
        return await this.foodService.addCollect(userId, collectData);
    }
};
exports.FoodController = FoodController;
__decorate([
    (0, swagger_1.ApiOperation)({ summary: '轮播图', description: '轮播图' }),
    (0, common_1.Get)('bannerAll'),
    (0, common_1.HttpCode)(common_1.HttpStatus.CREATED),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], FoodController.prototype, "bannerAll", null);
__decorate([
    (0, swagger_1.ApiOperation)({ summary: '美食列表', description: '美食列表' }),
    (0, common_1.Get)('list'),
    (0, common_1.HttpCode)(common_1.HttpStatus.CREATED),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], FoodController.prototype, "list", null);
__decorate([
    (0, swagger_1.ApiOperation)({ summary: '用户收藏', description: '用户收藏' }),
    (0, common_1.Get)('userconllect/:userId'),
    (0, common_1.HttpCode)(common_1.HttpStatus.CREATED),
    __param(0, (0, common_1.Param)("userId")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], FoodController.prototype, "userconllect", null);
__decorate([
    (0, swagger_1.ApiOperation)({ summary: '详情页', description: '详情页' }),
    (0, common_1.Get)('detail/:foodsid'),
    (0, common_1.HttpCode)(common_1.HttpStatus.CREATED),
    __param(0, (0, common_1.Param)("foodsid")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], FoodController.prototype, "detailfoods", null);
__decorate([
    (0, swagger_1.ApiOperation)({ summary: '搜索', description: '搜索' }),
    (0, common_1.HttpCode)(common_1.HttpStatus.CREATED),
    (0, common_1.Get)('search'),
    __param(0, (0, common_1.Query)('query')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], FoodController.prototype, "searchFoods", null);
__decorate([
    (0, swagger_1.ApiOperation)({ summary: '收藏删除', description: '收藏删除' }),
    (0, common_1.HttpCode)(common_1.HttpStatus.CREATED),
    (0, common_1.Delete)('collect/:id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Query)('userId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], FoodController.prototype, "deleteCollect", null);
__decorate([
    (0, swagger_1.ApiOperation)({ summary: '收藏', description: '收藏' }),
    (0, common_1.HttpCode)(common_1.HttpStatus.CREATED),
    (0, common_1.Post)('collect'),
    __param(0, (0, common_1.Query)('userId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], FoodController.prototype, "addCollect", null);
exports.FoodController = FoodController = __decorate([
    (0, swagger_1.ApiTags)('今日美食'),
    (0, common_1.Controller)('food'),
    __metadata("design:paramtypes", [Food_service_1.FoodService])
], FoodController);
//# sourceMappingURL=food.controller.js.map