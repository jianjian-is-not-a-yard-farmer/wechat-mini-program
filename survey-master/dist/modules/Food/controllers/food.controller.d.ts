import { FoodService } from '../services/Food.service';
import { ListEntity } from '../entities/List.entity';
import { ConllectEntity } from '../entities/Conllect.entity';
export declare class FoodController {
    private readonly foodService;
    constructor(foodService: FoodService);
    bannerAll(): Promise<{
        data: import("../entities/Banner.entity").BannerEntity[];
    }>;
    list(): Promise<{
        data: ListEntity[];
    }>;
    userconllect(userId: any): Promise<ConllectEntity[]>;
    detailfoods(foodsid: any): Promise<import("../entities/Detail.entity").DetailEntity[]>;
    searchFoods(query: any): Promise<ListEntity[]>;
    deleteCollect(id: string, userId: string): Promise<any>;
    addCollect(userId: string, collectData: Partial<ConllectEntity>): Promise<ConllectEntity>;
}
