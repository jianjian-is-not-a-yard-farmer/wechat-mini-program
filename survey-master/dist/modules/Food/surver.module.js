"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SurvetyModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const core_1 = require("@nestjs/core");
const survey_controller_js_1 = require("./controllers/survey.controller.js");
const Survey_entity_js_1 = require("./entities/Survey.entity.js");
const Survey_service_js_1 = require("../Survey/services/Survey.service.js");
const SurveyQuestion_entity_js_1 = require("./entities/SurveyQuestion.entity.js");
const Question_entity_js_1 = require("./entities/Question.entity.js");
const Response_entity_js_1 = require("./entities/Response.entity.js");
const Option_entity_js_1 = require("./entities/Option.entity.js");
let SurvetyModule = class SurvetyModule {
};
exports.SurvetyModule = SurvetyModule;
exports.SurvetyModule = SurvetyModule = __decorate([
    (0, common_1.Module)({
        imports: [
            core_1.RouterModule.register([
                {
                    path: 'survey',
                    module: this,
                },
            ]),
            typeorm_1.TypeOrmModule.forFeature([Survey_entity_js_1.SurveyEntity, SurveyQuestion_entity_js_1.SurveyQuestionEntity, Question_entity_js_1.QuestionEntity, Response_entity_js_1.ResponseEntity, Option_entity_js_1.OptionEntity]),
        ],
        controllers: [survey_controller_js_1.SurveyController],
        providers: [Survey_service_js_1.SurveyService],
        exports: [Survey_service_js_1.SurveyService],
    })
], SurvetyModule);
//# sourceMappingURL=surver.module.js.map