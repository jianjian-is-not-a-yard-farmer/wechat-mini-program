import { UserEntity } from '../User/User.entity';
export declare class JwtService {
    sign(payload: UserEntity): Promise<string>;
    verify(token: string): Promise<any>;
}
