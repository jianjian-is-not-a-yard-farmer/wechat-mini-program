"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WxService = void 0;
const common_1 = require("@nestjs/common");
const axios_1 = __importDefault(require("axios"));
let WxService = class WxService {
    constructor() {
        this.appid = 'wxecebe41f54635828';
        this.secret = '44f69bbbcb6208c40edce94736f7ffaf';
    }
    async code2Session(loginCode) {
        const url = `https://api.weixin.qq.com/sns/jscode2session?appid=${this.appid}&secret=${this.secret}&js_code=${loginCode}&grant_type=authorization_code`;
        const resWx = await axios_1.default.get(url);
        console.log("response from wx", resWx);
        const data = resWx === null || resWx === void 0 ? void 0 : resWx.data;
        const openid = data === null || data === void 0 ? void 0 : data.openid;
        const session_key = data === null || data === void 0 ? void 0 : data.session_key;
        return {
            openId: openid,
            sessionKey: session_key
        };
    }
    async getUserPhoneNumber(tmpCode) {
        const tokenUrl = `https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=${this.appid}&secret=${this.secret}`;
        try {
            const wxToken = await axios_1.default.get(tokenUrl);
            if (wxToken.data && wxToken.data.access_token) {
                const accessToken = wxToken.data.access_token;
                console.log("get wxToken before query phoneNumber: ", accessToken);
                console.log("get code before query phoneNumber: ", tmpCode);
                const getPhoneUrl = `https://api.weixin.qq.com/wxa/business/getuserphonenumber?access_token=${accessToken}`;
                const phoneData = await axios_1.default.post(getPhoneUrl, { code: tmpCode });
                if (phoneData.data && phoneData.data.errmsg === 'ok') {
                    const phoneNumber = phoneData.data.phone_info.phoneNumber;
                    console.log("User phone number:", phoneNumber);
                    return phoneNumber;
                }
                else {
                    console.error("Failed to get user phone number:", phoneData.data);
                    return null;
                }
            }
            else {
                console.error("Failed to get wxToken:", wxToken.data);
                return null;
            }
        }
        catch (error) {
            console.error("Error occurred:", error);
            return null;
        }
    }
};
exports.WxService = WxService;
exports.WxService = WxService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [])
], WxService);
//# sourceMappingURL=wx.service.js.map