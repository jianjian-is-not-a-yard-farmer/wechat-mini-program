export declare class WxService {
    appid: string;
    secret: string;
    constructor();
    code2Session(loginCode: string): Promise<{
        openId: any;
        sessionKey: any;
    }>;
    getUserPhoneNumber(tmpCode: string): Promise<any>;
}
