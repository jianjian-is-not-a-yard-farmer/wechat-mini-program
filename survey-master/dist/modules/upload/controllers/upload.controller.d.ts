/// <reference types="multer" />
import { UploadService } from '../services/upload.service';
import { AddfoodsEntity } from '@src/modules/Food/entities/Addfoods.entity';
export declare class UploadController {
    private readonly uploadService;
    constructor(uploadService: UploadService);
    uploadFile(file: Express.Multer.File, foods_title: string, userid: string): Promise<AddfoodsEntity>;
    updateFood(foods_id: string, id: string, file: Express.Multer.File, foods_title: string): Promise<AddfoodsEntity>;
    addlist(userId: any): Promise<AddfoodsEntity[]>;
    addlistid(id: any): Promise<AddfoodsEntity[]>;
}
