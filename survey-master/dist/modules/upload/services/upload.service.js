"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UploadService = void 0;
const common_1 = require("@nestjs/common");
const path_1 = require("path");
const fs_extra_1 = require("fs-extra");
const Addfoods_entity_1 = require("../../Food/entities/Addfoods.entity");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const User_entity_1 = require("../../User/User.entity");
let UploadService = class UploadService {
    constructor(addfoodsRepository, userRepository) {
        this.addfoodsRepository = addfoodsRepository;
        this.userRepository = userRepository;
        this.uploadPath = (0, path_1.join)(process.cwd(), 'uploads');
        (0, fs_extra_1.ensureDir)(this.uploadPath);
    }
    async uploadFile(file, foods_title, userId) {
        const user = await this.userRepository.findOne({ where: { id: userId } });
        console.log(user + "aaaaaaaaaaaaaaaaaaaaaaa");
        if (!user && user != userId) {
            throw new Error("用户id为空或不存在");
        }
        else {
            const filePath = (0, path_1.join)(this.uploadPath, file.originalname);
            await (0, fs_extra_1.writeFile)(filePath, file.buffer);
            const imageUrl = `http://www.localhost:4000/uploads/${file.originalname}`;
            const foods = new Addfoods_entity_1.AddfoodsEntity();
            foods.foods_img = imageUrl;
            foods.foods_title = foods_title;
            foods.userId = userId;
            return this.addfoodsRepository.save(foods);
        }
    }
    async updateFood(foodsId, foods_title, file, userId) {
        const food = await this.addfoodsRepository.findOne({ where: { id: foodsId, userId } });
        console.log(userId);
        console.log(foodsId);
        if (!food) {
            throw new common_1.NotFoundException("菜品不存在或用户无权限修改");
        }
        if (file) {
            const filePath = (0, path_1.join)(this.uploadPath, file.originalname);
            await (0, fs_extra_1.writeFile)(filePath, file.buffer);
            food.foods_img = `http://www.localhost:4000/uploads/${file.originalname}`;
        }
        food.foods_title = foods_title;
        return this.addfoodsRepository.save(food);
    }
    async addlist(userId) {
        const addlist = await this.addfoodsRepository.find({
            where: { userId }
        });
        if (!addlist) {
            throw new common_1.NotFoundException("userId不存在");
        }
        return addlist;
    }
    async addlistid(id) {
        return await this.addfoodsRepository.find({
            where: { id }
        });
    }
};
exports.UploadService = UploadService;
exports.UploadService = UploadService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(Addfoods_entity_1.AddfoodsEntity)),
    __param(1, (0, typeorm_1.InjectRepository)(User_entity_1.UserEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository])
], UploadService);
//# sourceMappingURL=upload.service.js.map