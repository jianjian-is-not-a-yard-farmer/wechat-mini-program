/// <reference types="multer" />
import { AddfoodsEntity } from '@src/modules/Food/entities/Addfoods.entity';
import { Repository } from 'typeorm';
import { UserEntity } from '@src/modules/User/User.entity';
export declare class UploadService {
    private readonly addfoodsRepository;
    private readonly userRepository;
    private readonly uploadPath;
    constructor(addfoodsRepository: Repository<AddfoodsEntity>, userRepository: Repository<UserEntity>);
    uploadFile(file: Express.Multer.File, foods_title: string, userId: string): Promise<AddfoodsEntity>;
    updateFood(foodsId: string, foods_title: string, file: Express.Multer.File, userId: string): Promise<AddfoodsEntity>;
    addlist(userId: any): Promise<AddfoodsEntity[]>;
    addlistid(id: any): Promise<AddfoodsEntity[]>;
}
