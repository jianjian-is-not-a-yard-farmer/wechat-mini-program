"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoadModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const core_1 = require("@nestjs/core");
const load_controller_1 = require("@src/modules/Load/controllers/load.controller");
let LoadModule = class LoadModule {
};
exports.LoadModule = LoadModule;
exports.LoadModule = LoadModule = __decorate([
    (0, common_1.Module)({
        imports: [
            core_1.RouterModule.register([
                {
                    path: 'load',
                    module: LoadModule,
                },
            ]),
            typeorm_1.TypeOrmModule.forFeature([]),
        ],
        controllers: [load_controller_1.LoadController],
        providers: [],
    })
], LoadModule);
//# sourceMappingURL=load.module.js.map