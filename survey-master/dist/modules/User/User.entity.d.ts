import { ConllectEntity } from "../Food/entities/Conllect.entity";
export declare class UserEntity {
    id: string;
    openid: string;
    userName: string;
    userPassword?: string;
    ative?: string;
    collections: ConllectEntity[];
}
