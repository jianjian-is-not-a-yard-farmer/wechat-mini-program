import { Repository } from 'typeorm';
import { UserEntity } from './User.entity';
import { JwtService } from '@nestjs/jwt/dist';
export declare class UserService {
    private readonly userRepository;
    private readonly jwtService;
    constructor(userRepository: Repository<UserEntity>, jwtService: JwtService);
    login(code: string): Promise<{
        access_token: string;
        user_id: string;
    }>;
    findOne(userId: string): Promise<UserEntity>;
    logina(user: any): Promise<{
        access_token: string;
    }>;
    adduser(userDto: any): Promise<any>;
    findOneByName(userName: string): Promise<UserEntity>;
    findAll(): Promise<UserEntity[]>;
    deleteUserByID(userID: string): Promise<any>;
}
