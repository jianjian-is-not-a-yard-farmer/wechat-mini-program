"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const User_service_1 = require("./User.service");
const queryUserList_vo_1 = require("./queryUserList.vo");
const jwt_auth_guard_1 = require("../auth/jwt-auth.guard");
const User_entity_1 = require("./User.entity");
let UserController = class UserController {
    constructor(userService) {
        this.userService = userService;
    }
    async queryUser(userId) {
        return this.userService.findOne(userId).then((data) => ({ data }));
    }
    async queryAllUser() {
        return this.userService.findAll().then((data) => ({ data }));
    }
    async login(code) {
        return this.userService.login(code);
    }
    async adduser(response) {
        return await this.userService.adduser(response);
    }
    async DeleteQueryBuilder(userID) {
        return this.userService.deleteUserByID(userID);
    }
};
exports.UserController = UserController;
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, swagger_1.ApiOperation)({ summary: '查询单个用户', description: '查询单个用户' }),
    (0, swagger_1.ApiOkResponse)({
        type: queryUserList_vo_1.QueryUserListVo,
        description: '查询单个用户',
    }),
    (0, common_1.Get)('list/:userId'),
    (0, common_1.HttpCode)(common_1.HttpStatus.CREATED),
    __param(0, (0, common_1.Param)('userId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "queryUser", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, swagger_1.ApiOperation)({ summary: '查询所有用户', description: '查询所有用户' }),
    (0, swagger_1.ApiOkResponse)({
        type: queryUserList_vo_1.QueryUserListVo,
        description: '查询所有用户',
    }),
    (0, common_1.Get)('queryAll'),
    (0, common_1.HttpCode)(common_1.HttpStatus.CREATED),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], UserController.prototype, "queryAllUser", null);
__decorate([
    (0, common_1.Post)('login'),
    (0, common_1.HttpCode)(common_1.HttpStatus.CREATED),
    __param(0, (0, common_1.Body)('code')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "login", null);
__decorate([
    (0, common_1.Post)('adduser'),
    (0, common_1.HttpCode)(common_1.HttpStatus.CREATED),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [User_entity_1.UserEntity]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "adduser", null);
__decorate([
    (0, swagger_1.ApiOperation)({ summary: '根据用户id删除', description: '根据用户id删除' }),
    (0, common_1.Get)('delete/:userID'),
    (0, common_1.HttpCode)(common_1.HttpStatus.OK),
    __param(0, (0, common_1.Param)('userID')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "DeleteQueryBuilder", null);
exports.UserController = UserController = __decorate([
    (0, swagger_1.ApiTags)('用户管理'),
    (0, common_1.Controller)('user'),
    __metadata("design:paramtypes", [User_service_1.UserService])
], UserController);
//# sourceMappingURL=User.controller.js.map