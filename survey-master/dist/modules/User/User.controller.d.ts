import { UserService } from './User.service';
import { UserEntity } from './User.entity';
export declare class UserController {
    private readonly userService;
    constructor(userService: UserService);
    queryUser(userId: string): Promise<{
        data: UserEntity;
    }>;
    queryAllUser(): Promise<{
        data: UserEntity[];
    }>;
    login(code: string): Promise<{
        access_token: string;
        user_id: string;
    }>;
    adduser(response: UserEntity): Promise<any>;
    DeleteQueryBuilder(userID: string): Promise<any>;
}
