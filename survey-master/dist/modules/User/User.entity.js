"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserEntity = void 0;
const swagger_1 = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const Conllect_entity_1 = require("../Food/entities/Conllect.entity");
let UserEntity = class UserEntity {
};
exports.UserEntity = UserEntity;
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", String)
], UserEntity.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ description: '微信ID' }),
    (0, typeorm_1.Column)({
        type: 'varchar',
        nullable: false,
        length: 128,
        name: 'openid',
        comment: '微信ID',
    }),
    __metadata("design:type", String)
], UserEntity.prototype, "openid", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ description: '用户名称' }),
    (0, typeorm_1.Column)({
        type: 'varchar',
        nullable: false,
        length: 128,
        name: 'userName',
        comment: '用户名称',
    }),
    __metadata("design:type", String)
], UserEntity.prototype, "userName", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ description: '登录密码' }),
    (0, typeorm_1.Column)({
        type: 'varchar',
        nullable: true,
        length: 128,
        name: 'userPassword',
        comment: '登录密码',
    }),
    __metadata("design:type", String)
], UserEntity.prototype, "userPassword", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ description: '头像' }),
    (0, typeorm_1.Column)({
        type: 'varchar',
        nullable: true,
        length: 128,
        name: 'ative',
        comment: '头像',
    }),
    __metadata("design:type", String)
], UserEntity.prototype, "ative", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Conllect_entity_1.ConllectEntity, collection => collection.user),
    __metadata("design:type", Array)
], UserEntity.prototype, "collections", void 0);
exports.UserEntity = UserEntity = __decorate([
    (0, typeorm_1.Entity)("user")
], UserEntity);
//# sourceMappingURL=User.entity.js.map