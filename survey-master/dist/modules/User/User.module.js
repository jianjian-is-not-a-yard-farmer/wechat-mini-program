"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModule = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const typeorm_1 = require("@nestjs/typeorm");
const User_entity_1 = require("./User.entity");
const User_controller_1 = require("./User.controller");
const User_service_1 = require("./User.service");
const wx_module_1 = require("../wx/wx.module");
const jwt_1 = require("@nestjs/jwt");
const jwt_strategy_1 = require("../auth/jwt.strategy");
const auth_service_1 = require("../auth/auth.service");
let UserModule = class UserModule {
};
exports.UserModule = UserModule;
exports.UserModule = UserModule = __decorate([
    (0, common_1.Module)({
        imports: [
            core_1.RouterModule.register([
                {
                    path: 'user',
                    module: this,
                }
            ]),
            typeorm_1.TypeOrmModule.forFeature([User_entity_1.UserEntity]),
            jwt_1.JwtModule.register({
                secret: "aaaaa",
                signOptions: { expiresIn: '8h' }
            }),
            wx_module_1.WxModule
        ],
        controllers: [User_controller_1.UserController],
        providers: [User_service_1.UserService, jwt_strategy_1.JwtStrategy, auth_service_1.AuthService]
    })
], UserModule);
//# sourceMappingURL=User.module.js.map