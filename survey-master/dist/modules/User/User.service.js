"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const User_entity_1 = require("./User.entity");
const dist_1 = require("@nestjs/jwt/dist");
const axios_1 = __importDefault(require("axios"));
let UserService = class UserService {
    constructor(userRepository, jwtService) {
        this.userRepository = userRepository;
        this.jwtService = jwtService;
    }
    async login(code) {
        const appId = 'wxa205dbfb55256747';
        const appSecret = 'b822e7cf94d83a400c8ec7fb8384bfb2';
        const response = await axios_1.default.get(`https://api.weixin.qq.com/sns/jscode2session?appid=${appId}&secret=${appSecret}&js_code=${code}&grant_type=authorization_code`);
        console.log(response);
        const { openid, session_key } = response.data;
        if (!openid || !session_key) {
            throw new Error('Failed to get openid or session_key');
        }
        let user = await this.userRepository.findOne({ where: { openid } });
        if (!user) {
            user = new User_entity_1.UserEntity();
            user.openid = openid;
            user.userName = "断弦如雪";
            user.ative = "https://tse4-mm.cn.bing.net/th/id/OIP-C.Y7F_S_i_3BeANSwOgmxSyQHaGy?w=192&h=180&c=7&r=0&o=5&dpr=1.3&pid=1.7";
            await this.userRepository.save(user);
        }
        const payload = { openid: user.openid, userid: user.id };
        return {
            access_token: this.jwtService.sign(payload),
            user_id: user.id
        };
    }
    async findOne(userId) {
        const user = await this.userRepository.findOne({
            where: {
                id: userId,
            },
        });
        if (!user) {
            throw new Error('user not found');
        }
        return user;
    }
    async logina(user) {
        const payload = { id: user.id, wxID: user.wxID, userName: user.userName, userPassword: user.userPassword };
        return {
            access_token: this.jwtService.sign(payload),
        };
    }
    async adduser(userDto) {
        const user = { wxID: userDto.wxID, userName: userDto.userName, userPassword: this.jwtService.sign(userDto.userPassword) };
        return await this.userRepository.save(user);
    }
    async findOneByName(userName) {
        const user = await this.userRepository.findOne({
            where: {
                userName,
            },
        });
        if (!user) {
            throw new Error('user not found -- findOneByName');
        }
        return user;
    }
    async findAll() {
        const users = await this.userRepository.find();
        return users;
    }
    async deleteUserByID(userID) {
        return await this.userRepository.delete({ id: userID });
    }
};
exports.UserService = UserService;
exports.UserService = UserService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(User_entity_1.UserEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        dist_1.JwtService])
], UserService);
//# sourceMappingURL=User.service.js.map