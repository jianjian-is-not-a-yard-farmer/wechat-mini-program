"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IS_DEV = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const swagger_1 = require("@nestjs/swagger");
require("dotenv/config");
const helmet_1 = __importDefault(require("helmet"));
const body_parser_1 = require("body-parser");
const app_module_1 = require("./app.module");
const http_exception_filter_1 = require("./filters/http-exception.filter");
const path_1 = require("path");
const express = __importStar(require("express"));
const PORT = process.env.PORT || 8080;
const PREFIX = '/api';
exports.IS_DEV = process.env.NODE_ENV !== 'production';
console.log(process.env);
async function bootstrap() {
    const logger = new common_1.Logger('main.ts');
    console.log(exports.IS_DEV, '是否为开发环境');
    const app = await core_1.NestFactory.create(app_module_1.AppModule, {
        logger: exports.IS_DEV ? ['log', 'debug', 'error', 'warn'] : ['error', 'warn'],
    });
    app.enableCors();
    app.use('/uploads', express.static((0, path_1.join)(process.cwd(), 'uploads')));
    app.setGlobalPrefix('api');
    if (exports.IS_DEV) {
        const options = new swagger_1.DocumentBuilder()
            .setTitle('忘忧美食小程序-api文档')
            .setBasePath(PREFIX)
            .addBearerAuth({ type: 'apiKey', in: 'header', name: 'token' })
            .setVersion('0.0.1')
            .build();
        const document = swagger_1.SwaggerModule.createDocument(app, options);
        swagger_1.SwaggerModule.setup(`${PREFIX}/docs`, app, document);
        swagger_1.SwaggerModule.setup('api', app, document);
    }
    app.use((0, helmet_1.default)());
    app.use((0, body_parser_1.json)({ limit: '50mb' }));
    app.use((0, body_parser_1.urlencoded)({ limit: '50mb', extended: true }));
    app.useGlobalFilters(new http_exception_filter_1.HttpExceptionFilter());
    await app.listen(PORT, () => {
        logger.debug(process.env);
        logger.log(`服务已经启动,接口请访问:http://wwww.localhost:${PORT}${PREFIX}`);
        logger.log(`服务已经启动,文档请访问:http://wwww.localhost:${PORT}${PREFIX}/docs`);
    });
}
bootstrap();
//# sourceMappingURL=main.js.map