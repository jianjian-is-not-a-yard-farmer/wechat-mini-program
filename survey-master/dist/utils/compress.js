"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.unzip = void 0;
const pako_1 = require("pako");
function unzip(b64Data) {
    if (b64Data) {
        const data = (0, pako_1.inflate)(b64Data);
        let res = '';
        const chunk = 8 * 1024;
        if (data) {
            for (let i = 0; i < data.length / chunk; i++) {
                res += String.fromCharCode.apply(null, data.slice(i * chunk, (i + 1) * chunk));
            }
            return decodeURIComponent(res);
        }
    }
    return '';
}
exports.unzip = unzip;
//# sourceMappingURL=compress.js.map