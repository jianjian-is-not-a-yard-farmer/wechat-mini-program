"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getUrlQuery = void 0;
const url_1 = require("url");
const getUrlQuery = (urlPath, key) => {
    const url = new url_1.URL(urlPath, 'https://www.');
    const params = new URLSearchParams(url.search.substring(1));
    return params.get(key);
};
exports.getUrlQuery = getUrlQuery;
//# sourceMappingURL=url.js.map