import { forwardRef,  Module } from '@nestjs/common';
import { WxService } from './wx.service';


@Module({
  imports: [],
  controllers: [],
  providers: [WxService],
  exports: [WxService],
})
export class WxModule { }
