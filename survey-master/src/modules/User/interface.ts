import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsInt, IsNotEmpty, MaxLength } from 'class-validator';

export class userDto {

    @ApiPropertyOptional({ required: true, description: "用户名称" })
    @IsNotEmpty({ message: '用户名称不能为空' })
    readonly userName: string;

    @ApiPropertyOptional({ required: true, description: "微信ID" })
    readonly wxID: string;

    @ApiPropertyOptional({ required: false, description: '用户密码' })
    readonly userPassword: string;

}