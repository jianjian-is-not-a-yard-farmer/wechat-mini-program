import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UserService } from './User.service';
import { QueryUserListVo } from './queryUserList.vo';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';



import { UserEntity } from './User.entity';

@ApiTags('用户管理')
@Controller('user')
export class UserController {

  constructor(
    private readonly userService: UserService,

  ) {
  }



  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: '查询单个用户', description: '查询单个用户' })
  @ApiOkResponse({
    type: QueryUserListVo,
    description: '查询单个用户',
  })
  @Get('list/:userId')
  @HttpCode(HttpStatus.CREATED)
  async queryUser(@Param('userId') userId: string) {
    return this.userService.findOne(userId).then((data) => ({ data }));
  }

  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: '查询所有用户', description: '查询所有用户' })
  @ApiOkResponse({
    type: QueryUserListVo,
    description: '查询所有用户',
  })
  @Get('queryAll')
  @HttpCode(HttpStatus.CREATED)
  async queryAllUser() {
    return this.userService.findAll().then((data) => ({ data }));
  }

  @Post('login')
  @HttpCode(HttpStatus.CREATED)
  async login(@Body('code') code: string) {
    return this.userService.login(code);
  }






  @Post('adduser')
  @HttpCode(HttpStatus.CREATED)
  async adduser(@Body() response: UserEntity): Promise<any> {
    return await this.userService.adduser(response)
  }


  // @UseGuards(JwtAuthGuard)
  // @ApiOperation({ summary: '查询用户电话号码', description: '查询用户电话号码' })
  // @ApiOkResponse({
  //   type: "",
  //   description: '查询用户电话号码',
  // })
  // @Get('query_user_phone_number')
  // @HttpCode(HttpStatus.CREATED)
  // async queryUserPhoneNumber(@Query('tmpCode') tmpCode: string) {
  //   return this.wxService.getUserPhoneNumber(tmpCode);
  // }


  /**
   * 添加用户
   */

  // @ApiOperation({ summary: '添加用户', description: '添加用户' })
  // @Post('addADuser')
  // @HttpCode(HttpStatus.CREATED)
  // async addAUser(@Body() aUser: userDto): Promise<any> {
  //   console.log(aUser);
  //   return this.userService.addAUser(aUser);

  // }


  @ApiOperation({ summary: '根据用户id删除', description: '根据用户id删除' })
  @Get('delete/:userID')
  @HttpCode(HttpStatus.OK)
  async DeleteQueryBuilder(@Param('userID') userID: string): Promise<any> {
    return this.userService.deleteUserByID(userID);
  }




}
