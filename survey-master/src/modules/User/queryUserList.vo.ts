import { ApiProperty } from '@nestjs/swagger';
import { UserEntity } from './User.entity';



export class QueryUserListVo {
  //用于方法,字段,表示对model属性的说明或者数据操作更改
  @ApiProperty({ description: '数据', type: UserEntity, isArray: true })
  data: UserEntity[];
}
