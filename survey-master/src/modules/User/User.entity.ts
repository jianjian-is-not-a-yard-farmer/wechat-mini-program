import { ApiPropertyOptional } from "@nestjs/swagger"
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm"
import { ConllectEntity } from "../Food/entities/Conllect.entity"
@Entity("user")
export class UserEntity {

    @PrimaryGeneratedColumn()
    id: string

    @ApiPropertyOptional({ description: '微信ID' })
    @Column({
        type: 'varchar',
        nullable: false,
        length: 128,
        name: 'openid',
        comment: '微信ID',
    })
    openid: string

    @ApiPropertyOptional({ description: '用户名称' })
    @Column({
        type: 'varchar',
        nullable: false,
        length: 128,
        name: 'userName',
        comment: '用户名称',
    })
    userName: string


    @ApiPropertyOptional({ description: '登录密码' })
    @Column({
        type: 'varchar',
        nullable: true,
        length: 128,
        name: 'userPassword',
        comment: '登录密码',
    })
    userPassword?: string

    @ApiPropertyOptional({ description: '头像' })
    @Column({
        type: 'varchar',
        nullable: true,
        length: 128,
        name: 'ative',
        comment: '头像',
    })
    ative?: string

    @OneToMany(() => ConllectEntity, collection => collection.user)
    collections: ConllectEntity[];

}