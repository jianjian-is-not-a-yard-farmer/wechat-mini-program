import { forwardRef, Global, Module } from '@nestjs/common';
import { RouterModule } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './User.entity';
import { UserController } from './User.controller';
import { UserService } from './User.service';
import { WxModule } from '../wx/wx.module';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from '../auth/jwt.strategy';
import { AuthService } from '../auth/auth.service';



@Module({
  imports: [
    RouterModule.register([
      {
        path: 'user',
        module: this,
      }
    ]),

    TypeOrmModule.forFeature([UserEntity]),
    JwtModule.register({
      secret: "aaaaa",
      signOptions: { expiresIn: '8h' }
    }),
    WxModule
  ],
  controllers: [UserController],
  providers: [UserService, JwtStrategy, AuthService]
})
export class UserModule { }
// 450922200211262527
// 仙湖第一附属
// // 
