import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {RouterModule} from '@nestjs/core';
import {SearchController} from "@src/modules/Search/controllers/search.controller";

@Module({
  imports: [
    RouterModule.register([
      {
        path: 'search',
        module: SearchModule,
      },
    ]),
    TypeOrmModule.forFeature([]),
  ],
  controllers: [SearchController],
  providers: [],
})
export class SearchModule {
}
