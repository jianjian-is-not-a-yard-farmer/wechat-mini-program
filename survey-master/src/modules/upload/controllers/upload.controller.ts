import {
    Body,
    Controller,
    Post,
    Request,
    UploadedFile,
    UseInterceptors,
    UseGuards,
    Put,
    Param,
    ParseIntPipe,
    Query,
    Get
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { UploadService } from '../services/upload.service';
import { AddfoodsEntity } from '@src/modules/Food/entities/Addfoods.entity';
import { AuthGuard } from '@nestjs/passport';
@Controller('upload')
export class UploadController {
    constructor(private readonly uploadService: UploadService) { }

    @Post()
    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(FileInterceptor('file'))
    async uploadFile(@UploadedFile() file: Express.Multer.File, @Body('foods_title') foods_title: string, @Body("userid") userid: string): Promise<AddfoodsEntity> {
        if (!file) {
            throw new Error('No file uploaded');
        }
        const userId = userid
        console.log("aaaaaaaaaa" + userId)
        return this.uploadService.uploadFile(file, foods_title, userId)
    }




    //修改上传
    @Post('update/:id')
    @UseInterceptors(FileInterceptor('file'))
    async updateFood(@Query('foods_id') foods_id: string, @Param('id', ParseIntPipe) id: string, @UploadedFile() file: Express.Multer.File, @Body('foods_title') foods_title: string) {

        return this.uploadService.updateFood(foods_id, foods_title, file, id);
    }


    //上传列表
    @Get("addlist/:userId")
    async addlist(@Param("userId") userId: any) {
        return this.uploadService.addlist(userId);
    }

    //根据foods_id
    @Get("addlistid/:id")
    async addlistid(@Param("id") id: any) {
        return this.uploadService.addlistid(id)
    }
}
