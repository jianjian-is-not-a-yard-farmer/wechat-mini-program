import { Module, MiddlewareConsumer, NestModule } from '@nestjs/common';
import { UploadController } from './controllers/upload.controller';
import { UploadService } from './services/upload.service';
import { join } from 'path';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as express from 'express';
import { AddfoodsEntity } from '../Food/entities/Addfoods.entity';
import { UserEntity } from '../User/User.entity';
@Module({
    imports: [TypeOrmModule.forFeature([AddfoodsEntity, UserEntity])],
    controllers: [UploadController],
    providers: [UploadService],
})
// export class UploadModule implements NestModule {
//     configure(consumer: MiddlewareConsumer) {
//         consumer
//             .apply(express.static(join(process.cwd(), 'uploads'))) // 使用 process.cwd() 获取项目根目录
//             .forRoutes('/');
//     }
// }
export class UploadModule { }
