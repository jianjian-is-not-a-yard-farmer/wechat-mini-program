import { ApiProperty } from '@nestjs/swagger';
import { BannerEntity } from './entities/Banner.entity';



export class FoodListVo {
    //用于方法,字段,表示对model属性的说明或者数据操作更改
    @ApiProperty({ description: '数据', type: BannerEntity, isArray: true })
    data: BannerEntity[];
}
