import { ApiPropertyOptional } from '@nestjs/swagger';
import { Entity, PrimaryGeneratedColumn, JoinColumn, Column, ManyToOne } from 'typeorm';

@Entity('banner')
export class BannerEntity {
    @PrimaryGeneratedColumn()
    id: String;


    @Column({
        type: 'varchar',
        nullable: false,
        name: 'foods_id',
        comment: '美食id，美食详情页专属',
    })
    foods_id: string;

    @Column({
        type: 'varchar',
        nullable: false,
        name: 'foods_banner',
        comment: '轮播图',
    })
    foods_banner: string;

}