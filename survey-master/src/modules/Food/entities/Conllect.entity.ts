import { ApiPropertyOptional } from '@nestjs/swagger';
import { Entity, PrimaryGeneratedColumn, JoinColumn, Column, ManyToOne } from 'typeorm';
import { UserEntity } from '@src/modules/User/User.entity';
@Entity('conllect')
export class ConllectEntity {
    @PrimaryGeneratedColumn()
    id: String;


    @Column({
        type: 'varchar',
        nullable: false,
        name: 'foods_id',
        comment: '美食id，美食详情页专属',
    })
    foods_id: string;

    @Column({
        type: 'varchar',
        nullable: false,
        name: 'foods_img',
        comment: '轮播图',
    })
    foods_img: string;

    @Column({
        type: 'varchar',
        nullable: false,
        name: 'foods_title',
        comment: '美食标题',
    })
    foods_title: string;

    @Column({
        type: 'varchar',
        nullable: false,
        name: 'foods_desc',
        comment: '简介',
    })
    foods_desc: string;

    @Column({
        type: 'varchar',
        nullable: false,
        name: 'name',
        comment: '作者',
    })
    name: string;

    @ManyToOne(() => UserEntity, user => user.collections)
    user: UserEntity;
}