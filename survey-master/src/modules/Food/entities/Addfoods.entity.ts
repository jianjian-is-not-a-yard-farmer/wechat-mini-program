import { ApiPropertyOptional } from '@nestjs/swagger';
import { Entity, PrimaryGeneratedColumn, JoinColumn, Column, ManyToOne } from 'typeorm';

@Entity('addfoods')
export class AddfoodsEntity {
    @PrimaryGeneratedColumn()
    id: String;


    // @Column({
    //     type: 'varchar',
    //     nullable: false,
    //     name: 'foods_id',
    //     comment: '美食id，美食详情页专属',
    // })
    // foods_id: string;
    @Column({
        type: 'varchar',
        nullable: false,
        name: 'foods_title',
        comment: '标题',
    })
    foods_title: string;

    @Column({
        type: 'varchar',
        nullable: false,
        name: 'foods_img',
        comment: '轮播图',
    })
    foods_img: string;
    @Column({
        type: 'varchar',
        nullable: false,
        name: 'userId',
        comment: '轮播图',
    })
    userId: string;

}