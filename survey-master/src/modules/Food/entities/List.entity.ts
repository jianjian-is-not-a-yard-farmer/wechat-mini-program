import { ApiPropertyOptional } from '@nestjs/swagger';
import { Entity, PrimaryGeneratedColumn, JoinColumn, Column, ManyToOne } from 'typeorm';

@Entity('list')
export class ListEntity {
    @PrimaryGeneratedColumn()
    id: String;


    @Column({
        type: 'varchar',
        nullable: false,
        name: 'foods_id',
        comment: '美食id，美食详情页专属',
    })
    foods_id: string;

    @Column({
        type: 'varchar',
        nullable: false,
        name: 'foods_img',
        comment: '图片',
    })
    foods_img: string;

    @Column({
        type: 'varchar',
        nullable: false,
        name: 'foods_title',
        comment: '美食标题',
    })
    foods_title: string;




}