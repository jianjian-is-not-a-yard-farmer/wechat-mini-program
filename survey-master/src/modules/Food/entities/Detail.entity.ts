import { ApiPropertyOptional } from '@nestjs/swagger';
import { Entity, PrimaryGeneratedColumn, JoinColumn, Column, ManyToOne } from 'typeorm';

@Entity('fooddetail')
export class DetailEntity {
    @PrimaryGeneratedColumn()
    id: String;


    @Column({
        type: 'varchar',
        nullable: false,
        name: 'foods_id',
        comment: '美食id，美食详情页专属',
    })
    foods_id: string;


    @Column({
        type: 'varchar',
        nullable: false,
        name: 'foods_title',
        comment: '美食标题',
    })
    foods_title: string;

    @Column({
        type: 'varchar',
        nullable: false,
        name: 'foods_difficulty',
        comment: '烹饪难度',
    })
    foods_difficulty: string;

    @Column({
        type: 'varchar',
        nullable: false,
        name: 'foods_time',
        comment: '烹饪难度',
    })
    foods_time: string;

    @Column({
        type: 'varchar',
        nullable: false,
        name: 'foods_ingredient',
        comment: '食材',
    })
    foods_ingredient: string;

    @Column({
        type: 'varchar',
        nullable: false,
        name: 'foods_procedure',
        comment: '步骤',
    })
    foods_procedure: string;
    @Column({
        type: 'varchar',
        nullable: false,
        name: 'foods_img',
        comment: '图片',
    })
    foods_img: string;

    @Column({
        type: 'varchar',
        nullable: false,
        name: 'foods_htmlimg',
        comment: '图片分享',
    })
    foods_htmlimg: string;

    @Column({
        type: 'varchar',
        nullable: false,
        name: 'foods_knack',
        comment: '烹饪小窍门',
    })
    foods_knack: string;

}