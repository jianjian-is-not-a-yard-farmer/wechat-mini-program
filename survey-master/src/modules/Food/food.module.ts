import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RouterModule } from '@nestjs/core';
import { BannerEntity } from './entities/Banner.entity';
import { FoodService } from '../Food/services/Food.service';
import { FoodController } from './controllers/food.controller';
import { ConllectEntity } from './entities/Conllect.entity';
import { UserEntity } from '../User/User.entity';
import { DetailEntity } from './entities/Detail.entity';
import { ListEntity } from './entities/List.entity';

@Module({
  imports: [
    RouterModule.register([
      {
        path: 'food',
        module: this,
      },
    ]),
    TypeOrmModule.forFeature([BannerEntity, ConllectEntity, UserEntity, DetailEntity, ListEntity]),
  ],
  controllers: [FoodController],
  providers: [FoodService],
  exports: [FoodService],
})
export class FoodModule {
}
