
import { Controller, Get, Post, Body, HttpCode, HttpStatus, Param, UseGuards, Query, Delete, Req } from '@nestjs/common';

import { ApiOperation, ApiOkResponse, ApiTags } from "@nestjs/swagger";


import { FoodService } from '../services/Food.service';
import { JwtAuthGuard } from '@src/modules/auth/jwt-auth.guard';
import { ListEntity } from '../entities/List.entity';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { ConllectEntity } from '../entities/Conllect.entity';
@ApiTags('今日美食')
@Controller('food')
export class FoodController {

    constructor(
        private readonly foodService: FoodService
    ) { }

    @ApiOperation({ summary: '轮播图', description: '轮播图' })
    @Get('bannerAll')
    @HttpCode(HttpStatus.CREATED)
    async bannerAll() {
        return this.foodService.findAll().then((data) => ({ data }))

    }
    @ApiOperation({ summary: '美食列表', description: '美食列表' })
    @Get('list')
    @HttpCode(HttpStatus.CREATED)
    async list() {
        return this.foodService.findAllList().then((data) => ({ data }))

    }




    // @UseGuards(JwtAuthGuard)
    @ApiOperation({ summary: '用户收藏', description: '用户收藏' })
    @Get('userconllect/:userId')
    @HttpCode(HttpStatus.CREATED)
    async userconllect(@Param("userId") userId: any) {
        const userIdNumber = parseInt(userId, 10);
        return this.foodService.findAllConllect(userIdNumber);

    }

    // @UseGuards(JwtAuthGuard)
    @ApiOperation({ summary: '详情页', description: '详情页' })
    @Get('detail/:foodsid')
    @HttpCode(HttpStatus.CREATED)
    async detailfoods(@Param("foodsid") foodsid: any) {
        return await this.foodService.findDetail(foodsid)

    }
    @ApiOperation({ summary: '搜索', description: '搜索' })

    @HttpCode(HttpStatus.CREATED)
    @Get('search')
    async searchFoods(@Query('query') query: any): Promise<ListEntity[]> {
        console.log('Received query:', query); // 打印调试信息
        return await this.foodService.searchFoods(query);
    }

    //需要改进
    @ApiOperation({ summary: '收藏删除', description: '收藏删除' })
    @HttpCode(HttpStatus.CREATED)
    @Delete('collect/:id')
    async deleteCollect(@Param('id') id: string, @Query('userId') userId: string) {

        return await this.foodService.deleteCollect(userId, id);

    }


    @ApiOperation({ summary: '收藏', description: '收藏' })
    @HttpCode(HttpStatus.CREATED)
    @Post('collect')
    async addCollect(@Query('userId') userId: string, @Body() collectData: Partial<ConllectEntity>) {

        return await this.foodService.addCollect(userId, collectData);
    }

}
