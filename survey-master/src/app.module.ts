import * as path from 'path';
import { ClassSerializerInterceptor, Module, Global } from '@nestjs/common';
import { APP_INTERCEPTOR, APP_PIPE, HttpAdapterHost } from '@nestjs/core';
import { ConfigModule, ConfigService } from 'nestjs-config';
import { ScheduleModule } from '@nestjs/schedule';

// import { LoadModule } from "@src/modules/Load/load.module";
// import { SearchModule } from "@src/modules/Search/search.module";
import { TypeOrmModule } from '@nestjs/typeorm';
// import { AuthModule } from './modules/auth/auth.module';
import { UserModule } from './modules/User/User.module';
// import { SurvetyModule } from '@src/modules/Survey/surver.module'
import { FoodModule } from './modules/Food/food.module';
import { JwtModule } from '@nestjs/jwt';
import { UploadModule } from './modules/upload/upload.module';

import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';



@Module({
  imports: [

    ScheduleModule.forRoot(),


    // 配置加载配置文件
    ConfigModule.load(path.resolve(__dirname, 'config', '**/!(*.d).{ts,js}'), {
      modifyConfigName: (name: string) => name.replace('.config', ''),
    }),

    // mysql的连接
    TypeOrmModule.forRootAsync({
      useFactory: async (config: ConfigService) => ({
        type: config.get('database.type'),
        host: config.get('database.host'),
        port: config.get('database.port'),
        username: config.get('database.username'),
        password: config.get('database.password'),
        database: config.get('database.database'),
        entities: [__dirname + '/**/*.entity{.ts,.js}'],
        logging: config.get('database.logging'),
        synchronize: true, // 同步数据库
        timezone: '+08:00', // 东八区
        charset: 'utf8mb4',
        cache: {
          duration: 60000, // 1分钟的缓存
        },
        extra: {
          poolMax: 32,
          poolMin: 16,
          queueTimeout: 60000,
          pollPingInterval: 60, // 每隔60秒连接
          pollTimeout: 60, // 连接有效60秒
        },
      }),
      inject: [ConfigService],
    }),

    // AuthModule,
    UploadModule,
    // LoadModule,
    // SearchModule,
    UserModule,
    JwtModule,
    FoodModule,
    // SurvetyModule
  ],
  controllers: [],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
  ],
})
export class AppModule { }
